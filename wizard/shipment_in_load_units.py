# -*- coding: utf-8 -*-
#This file is part stock_anmat_traceability module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from datetime import datetime

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction

__all__ = ['LoadUnitsStart', 'LoadUnitsStartLine', 'LoadUnits']


class LoadUnitsStart(ModelView):
    'Load Units Start'
    __name__ = 'stock.shipment.in.load.units.start'

    from_location = fields.Many2One("stock.location", "From Location",
        select=True, required=True, domain=[
            ('type', 'not in', ['warehouse', 'view']),
            ])
    to_location = fields.Many2One("stock.location", "To Location",
        select=True, required=True, domain=[
            ('type', 'not in', ['warehouse', 'view']),
            ])
    units_qty = fields.Integer('Units Quantity', readonly=True)
    lines = fields.One2Many('stock.shipment.in.load.units.start.line',
        'load_unit', 'Numbers')

    @classmethod
    def __setup__(cls):
        super(LoadUnitsStart, cls).__setup__()
        cls._error_messages.update({
            'number_already_loaded': 'Number already loaded',
        })

    @staticmethod
    def default_from_location():
        ShipmentIn = Pool().get('stock.shipment.in')
        shipment_id = Transaction().context.get('active_id')
        if shipment_id:
            shipment = ShipmentIn.browse([shipment_id])
            return shipment[0].supplier_location.id if shipment else None
        return None

    @staticmethod
    def default_to_location():
        ShipmentIn = Pool().get('stock.shipment.in')
        shipment_id = Transaction().context.get('active_id')
        if shipment_id:
            shipment = ShipmentIn.browse([shipment_id])
            return shipment[0].warehouse_input.id if shipment else None
        return None

    @fields.depends('lines')
    def on_change_lines(self):
        numbers = [line.serial_number for line in self.lines
            if line.product and line.product.anmat_traceable]
        products = [line.product.id for line in self.lines
            if line.product and not line.product.anmat_traceable]
        self.units_qty = len(numbers) + len(products)
        warning = self.check_numbers(numbers)
        if warning:
            self.raise_user_error(warning)

    @staticmethod
    def check_numbers(numbers):
        if len(numbers) > len(set(numbers)):
            return 'number_already_loaded'
        return None


class LoadUnitsStartLine(ModelView):
    'Load Units Start Lines'
    __name__ = 'stock.shipment.in.load.units.start.line'

    load_unit = fields.Many2One('stock.shipment.in.load.units.start',
        'Load Unit', required=True, ondelete='CASCADE')
    serial_number = fields.Char('Number')
    product = fields.Many2One('product.product', 'Product',
        domain=[
            ('anmat_traceable', '=', True),
            ])
    lot = fields.Char('Lot')
    expiration_date = fields.Date('Expiration Date')

    @classmethod
    def __setup__(cls):
        super(LoadUnitsStartLine, cls).__setup__()
        cls._error_messages.update({
            'product_not_found': 'No product matches GTIN: %s',
        })

    @fields.depends('serial_number')
    def on_change_serial_number(self):
        '''
        -ANMAT traceable product code to be parsed:
          Mandatory: 01 + Product GTIN (14) + 21 + Serial Number (10)
          Optional: 10 + Lot (10) + 17 + YYMMDD (expiration date)
          Example: 010773094904669421234219217510P00289A41217190831
        -Common product code (no serial number):
          01 + Product GTIN (14)
          + 17 + YYMMDD (expiration date) [OR] + 10 + Lot (3-6)
          + 90 + Code (7)
          Examples:
            01077962852802601721033110442890RQ8C793
            0107798032934662106041719063090ROAA012
        -Other products codes with length < 36 
          01 + Product GTIN (077962852 + 5 DIGITS) + 1710 
          Example:
            01077962852840081710 
          
        '''
        Product = Pool().get('product.product')
        Lot = Pool().get('stock.lot')
        code = ''.join([x for x in self.serial_number if x.isalnum()])        
        if len(code) > 19:
            gtin_indicator_digits = code[:2]
            gtin = code[2:16]
            product = Product.search([
                ('anmat_gtin_indicator_digits','=',gtin_indicator_digits),
                ('anmat_gtin', '=', gtin),
                ])
            if product:
                self.product = product[0].id
            else:
                self.raise_user_error('product_not_found', gtin)

            if self.product.anmat_traceable:
                # Traceable
                self.serial_number = code[18:28]
                if code[28:30] == '10':
                    if code[-8:-6] == '17':
                        self.expiration_date = datetime.strptime(
                            code[-6:], '%y%m%d')
                        self.lot = code[30:-8]
                elif code[28:30] == '17':
                    self.expiration_date = datetime.strptime(
                        code[30:36], '%y%m%d')
                    if code[36:38] == '10':
                        self.lot = code[38:]
            else:
                # Not traceable
                if code[16:18] == '17' and code[18:20] != '10':
                    self.expiration_date = datetime.strptime(
                        code[18:24], '%y%m%d')
                    if code[24:26] == '10':
                        self.lot = code[26:-9]
                elif code[16:18] == '10':
                    self.lot = code[18:-17]
                    if code[-17:-15] == '17':
                        self.expiration_date = datetime.strptime(
                            code[-15:-9], '%y%m%d')
                elif code[16:20] == '1710':
                    lot = Lot.search([('product','=',product[0].id)])
                    if lot:
                        self.lot = lot[-1].number
                        self.expiration_date = lot[-1].expiration_date
                self.serial_number = None
        #elif len(code) < 36:
            #gtin_indicator_digits = code[:2]
            #gtin = code[2:16]
            #product = Product.search([
                #('anmat_gtin_indicator_digits','=',gtin_indicator_digits),
                #('anmat_gtin', '=', gtin),
                #])
            #if product:
                #self.product = product[0].id
            #else:
                #self.raise_user_error('product_not_found', gtin)
                
                


class LoadUnits(Wizard):
    'Load Units'
    __name__ = 'stock.shipment.in.load.units'

    start = StateView('stock.shipment.in.load.units.start',
        'stock_anmat_traceability.load_units_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Load', 'load', 'tryton-ok', default=True),
            ])
    load = StateTransition()

    def transition_load(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Lot = pool.get('stock.lot')
        Unit = pool.get('stock.lot.unit')

        shipment = 'stock.shipment.in,' \
            + str(Transaction().context['active_id'])
        lines = []
        for line in self.start.lines:
            if not line.product:
                continue
            data = {
                'shipment': shipment,
                'from_location': self.start.from_location.id,
                'to_location': self.start.to_location.id,
                'product': line.product.id,
                'uom': line.product.default_uom.id,
                'unit_price': line.product.cost_price,
                }
            #TODO change to make usable with all products. See if there is a module
            # that does the same
            if line.product.anmat_traceable:
                if len(line.serial_number) < 1:
                    continue
                lot = Lot.search([
                    ('product', '=', line.product.id),
                    ('number', '=', line.lot),
                    ])
                if not lot:
                    lot = Lot.create([{
                        'product': line.product.id,
                        'number': line.lot,
                        'expiration_date': line.expiration_date,
                        }])
                unit = Unit.create([{
                    'lot': lot[0].id,
                    'serial_number': line.serial_number,
                    }])
                if unit:
                    data['lot'] = lot[0].id
                    data['anmat_unit'] = unit[0].id
                    data['quantity'] = line.product.qty_in
                    lines.append(data)
            else:
                lot = Lot.search([
                    ('product', '=', line.product.id),
                    ('number', '=', line.lot),
                    ])
                if not lot:
                    lot = Lot.create([{
                        'product': line.product.id,
                        'number': line.lot,
                        'expiration_date': line.expiration_date,
                        }])
                data['lot'] = lot[0].id
                data['quantity'] = int(line.product.qty_in)
                lines.append(data)

        Move.create(lines)
        return 'end'
