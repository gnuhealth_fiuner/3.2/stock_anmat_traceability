# -*- coding: utf-8 -*-
#This file is part stock_anmat_traceability module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from ..anmat import AnmatTraceWS

from trytond.model import ModelView, fields
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction

__all__ = ['ShipmentOutInformStart', 'ShipmentOutInformResult',
    'ShipmentOutInform']


class ShipmentOutInformStart(ModelView):
    'Shipment Out Inform Start'
    __name__ = 'stock.shipment.out.inform.start'

    info = fields.Text('Info', readonly=True)


class ShipmentOutInformResult(ModelView):
    'Shipment Out Inform Result'
    __name__ = 'stock.shipment.out.inform.result'

    shipments_succeeded = fields.Many2Many('stock.shipment.out', None, None,
        'Shipments succeded', readonly=True, states={
            'invisible': ~Eval('shipments_succeeded'),
            })
    shipments_failed = fields.Many2Many('stock.shipment.out', None, None,
        'Shipments failed', readonly=True, states={
            'invisible': ~Eval('shipments_failed'),
            })
    info = fields.Text('Info', readonly=True)


class ShipmentOutInform(Wizard):
    'Shipment Out Inform'
    __name__ = 'stock.shipment.out.inform'

    start_state = 'start'
    start = StateView('stock.shipment.out.inform.start',
        'stock_anmat_traceability.shipment_out_inform_start_view', [
            Button('Inform', 'inform', 'tryton-ok', default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    inform = StateTransition()
    result = StateView('stock.shipment.out.inform.result',
        'stock_anmat_traceability.shipment_out_inform_result_view', [
            Button('Ok', 'end', 'tryton-ok', True),
            ])

    @classmethod
    def __setup__(cls):
        super(ShipmentOutInform, cls).__setup__()
        cls._error_messages.update({
                'service_unavailable': ('The ANMAT service is unavailable, '
                    'try again later.'),
                })

    @staticmethod
    def default_start(fields):
        return {
            'info': 'Se informarán los movimientos a ANMAT',
            }

    def transition_inform(self):
        Shipment = Pool().get('stock.shipment.out')

        shipments_succeeded = []
        shipments_failed = []
        shipments = Shipment.browse(Transaction().context.get('active_ids'))
        ws = AnmatTraceWS()
        for shipment in shipments:
            if shipment.state != 'done':
                continue
            try:
                if ws.send(shipment) is True:
                    shipments_succeeded.append(shipment.id)
                else:
                    shipments_failed.append(shipment.id)
            except Exception as e:
                if hasattr(e, 'faultstring') \
                        and hasattr(e.faultstring, 'find'):
                    if e.faultstring.find('SERVICE_UNAVAILABLE') \
                            or e.faultstring.find('MS_UNAVAILABLE') \
                            or e.faultstring.find('TIMEOUT') \
                            or e.faultstring.find('SERVER_BUSY'):
                        self.raise_user_error('service_unavailable')
                raise
        self.result.shipments_succeeded = shipments_succeeded
        self.result.shipments_failed = shipments_failed
        self.result.info = ws.messages
        return 'result'

    def default_result(self, fields):
        return {
            'shipments_succeeded': [p.id
                for p in self.result.shipments_succeeded],
            'shipments_failed': [p.id for p in self.result.shipments_failed],
            'info': self.result.info,
            }
