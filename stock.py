# -*- coding: utf-8 -*-
# This file is part stock_anmat_traceability module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from collections import defaultdict

from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.pyson import Eval, Bool, Not
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.tools import grouped_slice
from trytond.modules.stock import StockMixin
from .anmat import AnmatTraceWS

__all__ = ['Unit', 'Move', 'ShipmentAnmatMixin', 'ShipmentIn', 'ShipmentOut',
    'ShipmentInReturn', 'ShipmentOutReturn', 'ShipmentInternal', 'Period',
    'PeriodCacheUnit', 'Inventory', 'InventoryLine']
__metaclass__ = PoolMeta

SUPPORTED_EVENTS = {
    'stock.shipment.in': ['2'],
    'stock.shipment.out': ['1', '8', '45'],
    'stock.shipment.in.return': ['5', '18', '20', '25'],
    'stock.shipment.out.return': ['6', '19', '21', '26'],
    'stock.shipment.internal': ['7', '9', '22', '23'],
}

NO_DESTINATION_EVENT_TYPES = [
    '7', '8', '9', '15', '16', '17', '22', '23',
    '31', '32', '36', '37', '38', '39', '42', '45', '50']


class Unit(ModelSQL, ModelView, StockMixin):
    "Stock Lot Unit"
    __name__ = 'stock.lot.unit'
    _rec_name = 'serial_number'

    serial_number = fields.Char('Number', required=True, select=True)
    lot = fields.Many2One('stock.lot', 'Lot', required=True)
    product = fields.Function(fields.Many2One('product.product', 'Product'),
        'get_product', searcher="search_product")
    quantity = fields.Function(fields.Integer('Quantity'), 'get_quantity',
        searcher='search_quantity')
    fractions = fields.Integer('Fractions', readonly=True)
    moves = fields.One2Many('stock.move', 'anmat_unit', 'Moves', readonly=True)

    @classmethod
    def get_product(cls, units, name):
        lines = {}
        for sub_units in grouped_slice(units):
            for unit in sub_units:
                lines[unit.id] = unit.lot.product.id
        return lines

    @classmethod
    def search_product(cls, name, clause):
        res = []
        res.append(('lot.product', clause[1], clause[2]))
        return res

    @classmethod
    def get_quantity(cls, units, name):
        Location = Pool().get('stock.location')

        location_ids = Transaction().context.get('locations')
        if not location_ids:

            locations = Location.search([
                ('type', '=', 'storage'),
                ('active', '=', True)]
                )
            location_ids = [l.id for l in locations]

        products = list(set(u.product for u in units))
        res = cls._get_quantity(units, name, location_ids, products,
            grouping=('product', 'anmat_unit'))
        for k, v in res.items():
            res[k] = int(v)
        return res

    @classmethod
    def search_quantity(cls, name, domain=None):
        location_ids = Transaction().context.get('locations')
        return cls._search_quantity(name, location_ids, domain,
            grouping=('product', 'anmat_unit'))

    @classmethod
    def create(cls, vlist):
        Lot = Pool().get('stock.lot')

        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            lot = Lot(vals['lot'])
            vals['fractions'] = lot.product.anmat_fractions
        return super(Unit, cls).create(vlist)


class Move (metaclass = PoolMeta):
    __name__ = 'stock.move'

    anmat_unit = fields.Many2One('stock.lot.unit', 'Unit',
        domain=[
            ('lot', '=', Eval('lot')),
            ],
        states={
            'readonly': Eval('state').in_(['cancel', 'done']),
            },
        depends=['state', 'lot'])
    anmat_transactions = fields.One2Many('anmat.trace.transaction',
        'move', 'ANMAT Transactions', readonly=True,
        states={
            'invisible': ~Eval('anmat_unit'),
            },
        depends=['anmat_unit'])

    @classmethod
    def __setup__(cls):
        super(Move, cls).__setup__()
        cls._error_messages.update({
            'qty_out_of_range': 'Quantity of unit %s can not be greater '
            'than %s.',
            'not_event_defined': 'It was not possible to define the event '
            'type for shipment with code %s.',
            })

    @classmethod
    def validate(cls, moves):
        super(Move, cls).validate(moves)
        for move in moves:
            if move.anmat_unit and move.quantity > move.anmat_unit.fractions:
                cls.raise_user_error('qty_out_of_range',
                    (move.anmat_unit.serial_number,
                    move.anmat_unit.fractions))

    @classmethod
    def write(cls, *args):
        pool = Pool()
        LocationConfig = pool.get('anmat.location.config')
        AnmatTransaction = pool.get('anmat.trace.transaction')

        super(Move, cls).write(*args)
        actions = iter(args)
        for moves in list(actions)[::2]:
            for move in moves:
                if not move.shipment or move.shipment.inform_anmat is False:
                    continue
                if move.state == 'done' and \
                        move.product.anmat_traceable is True:
                    event = move.get_anmat_event()
                    if not event:
                        cls.raise_user_error('not_event_defined',
                            (move.shipment.code,))
                    event_type = event.type.code
                    # Fractionable units should be completely consumed
                    if event_type == '45' and move.anmat_unit.quantity > 0:
                        continue
                    locations_config = LocationConfig.search([
                        ('from_location', '=', move.from_location.id),
                        ('to_location', '=', move.to_location.id),
                        ])
                    locations_event_types = [str(l.event_type.code)
                        for l in locations_config]
                    if event_type not in locations_event_types:
                        continue
                    model = str(move.shipment)
                    shipment_type = model[:model.find(',')]
                    supported_events = SUPPORTED_EVENTS.get(shipment_type, [])
                    if event_type not in supported_events:
                        continue

                    anmat_transaction = {
                        'move': move.id,
                        'date': move.effective_date,
                        'event': event.id if event else None,
                    }
                    AnmatTransaction.create([anmat_transaction])

    def get_anmat_event(self):
        pool = Pool()
        Event = pool.get('anmat.event')

        if not self.shipment:
            return None
        company_type = self.company.party.anmat_agent_type
        model = str(self.shipment)
        shipment_type = model[:model.find(',')]
        Shipment = pool.get(shipment_type)
        clause = None

        if shipment_type == 'stock.shipment.in':
            shipment = Shipment.browse([self.shipment.id])[0]
            supplier_type = shipment.supplier.anmat_agent_type
            event_type = '2'
            clause = [
                ('type.code', '=', event_type),
                ('notifier', '=', company_type.id),
                ('origin', '=', supplier_type.id),
                ]
            if event_type not in NO_DESTINATION_EVENT_TYPES:
                clause.append(('destination', '=', company_type.id))
        elif shipment_type == 'stock.shipment.out':
            shipment = Shipment.browse([self.shipment.id])[0]
            customer_type = shipment.customer.anmat_agent_type
            event_type = '1'
            # Check if customer is a patient
            if customer_type.code == '10':
                event_type = '8'
            # Check if is a fractionable unit
            if self.product.anmat_fractionable:
                event_type = '45'
            clause = [
                ('type.code', '=', event_type),
                ('notifier', '=', company_type.id),
                ('origin', '=', company_type.id),
                ]
            if event_type not in NO_DESTINATION_EVENT_TYPES:
                clause.append(('destination', '=', customer_type.id))
        elif shipment_type == 'stock.shipment.in.return':
            shipment = Shipment.browse([self.shipment.id])[0]
            supplier_type = shipment.customer.anmat_agent_type
            reason = {
                'return': '5',
                'expiration': '18',
                'prohibition': '20',
                'quarantine': '25',
                }
            event_type = reason.get(shipment.anmat_reason)
            clause = [
                ('type.code', '=', event_type),
                ('notifier', '=', company_type.id),
                ('origin', '=', company_type.id),
                ]
            if event_type not in NO_DESTINATION_EVENT_TYPES:
                clause.append(('destination', '=', supplier_type.id))
        elif shipment_type == 'stock.shipment.out.return':
            shipment = Shipment.browse([self.shipment.id])[0]
            customer_type = shipment.customer.anmat_agent_type
            reason = {
                'return': '6',
                'expiration': '19',
                'prohibition': '21',
                'quarantine': '26',
                }
            event_type = reason.get(shipment.anmat_reason)
            clause = [
                ('type.code', '=', event_type),
                ('notifier', '=', company_type.id),
                ('origin', '=', customer_type.id),
                ]
            if event_type not in NO_DESTINATION_EVENT_TYPES:
                clause.append(('destination', '=', company_type.id))
        elif shipment_type == 'stock.shipment.internal':
            shipment = Shipment.browse([self.shipment.id])[0]
            reason = {
                'damaged', '7',
                'stolen_missing', '9',
                'prohibition', '22',
                'expiration', '23',
                }
            event_type = reason.get(shipment.anmat_reason)
            clause = [
                ('type.code', '=', event_type),
                ('notifier', '=', company_type.id),
                ('origin', '=', company_type.id),
                ]

        if clause:
            events = Event.search(clause)
            if events:
                return events[0]
        return None


class ShipmentAnmatMixin (metaclass = PoolMeta):
    inform_anmat = fields.Boolean('Inform ANMAT',
        states={'readonly': Eval('state').in_(['cancel', 'done'])},
        help='If not checked, no communication will be made with ANMAT')
    anmat_transactions = fields.Function(fields.One2Many(
        'anmat.trace.transaction', 'move', 'ANMAT Transactions'),
        'get_anmat_transactions', setter='set_anmat_transactions')

    @staticmethod
    def default_inform_anmat():
        return False

    def get_anmat_transactions(self, name):
        AnmatTrace = Pool().get('anmat.trace.transaction')
        transactions = AnmatTrace.search([
            ('move', 'in', [m.id for m in self.moves]),
            ])
        return [t.id for t in transactions]

    @classmethod
    def set_anmat_transactions(cls, transactions, name, value):
        pass


class ShipmentIn(ShipmentAnmatMixin):
    __name__ = 'stock.shipment.in'

    @classmethod
    def __setup__(cls):
        super(ShipmentIn, cls).__setup__()
        cls._buttons.update({
                'load_units_wizard': {
                    'readonly': ~Eval('state').in_(['draft']),
                    },
                })

    @classmethod
    def view_attributes(cls):
        return [('/form/notebook/page[@id="anmat_transactions"]',
            'states', {
                'invisible': Not(Bool(Eval('anmat_transactions')))
                })]

    @classmethod
    @ModelView.button_action('stock_anmat_traceability.wizard_load_units')
    def load_units_wizard(cls, shipments):
        pass

    @classmethod
    def _get_inventory_moves(cls, incoming_move):
        move = super(ShipmentIn, cls)._get_inventory_moves(incoming_move)
        if move and incoming_move.anmat_unit:
            move.anmat_unit = incoming_move.anmat_unit
        return move

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, shipments):
        super(ShipmentIn, cls).done(shipments)
        cls.anmat_confirm(shipments)

    @classmethod
    def anmat_confirm(cls, shipments):
        pass
        ws = AnmatTraceWS()
        for shipment in shipments:
            ws.confirm(shipment)


class ShipmentOut(ShipmentAnmatMixin):
    __name__ = 'stock.shipment.out'

    @classmethod
    def __setup__(cls):
        super(ShipmentOut, cls).__setup__()
        cls._buttons.update({
                'pick_units_wizard': {
                    'readonly': ~Eval('state').in_(['draft']),
                    },
                })

    @classmethod
    def view_attributes(cls):
        return [('/form/notebook/page[@id="anmat_transactions"]',
            'states', {
                'invisible': Not(Bool(Eval('anmat_transactions')))
                })]

    @classmethod
    @ModelView.button_action('stock_anmat_traceability.wizard_pick_units')
    def pick_units_wizard(cls, shipments):
        pass

    def _get_inventory_move(self, move):
        move_obj = super(ShipmentOut, self)._get_inventory_move(move)
        move_obj.lot = move.lot
        move_obj.anmat_unit = move.anmat_unit
        return move_obj

    @classmethod
    def _sync_inventory_to_outgoing(cls, shipments, create=True, write=True):
        pool = Pool()
        Uom = pool.get('product.uom')
        Move = pool.get('stock.move')
        super(ShipmentOut, cls)._sync_inventory_to_outgoing(
            shipments, create=create, write=write)
        for shipment in shipments:
            outgoing_by_product = defaultdict(list)
            for move in shipment.outgoing_moves:
                outgoing_by_product[move.product.id].append(move)
            for move in shipment.inventory_moves:
                if not move.anmat_unit:
                    continue
                quantity = Uom.compute_qty(move.uom, move.quantity,
                    move.product.default_uom, round=False)
                outgoing_moves = outgoing_by_product[move.product.id]
                while outgoing_moves and quantity > 0:
                    out_move = outgoing_moves.pop()
                    out_quantity = Uom.compute_qty(out_move.uom,
                        out_move.quantity, out_move.product.default_uom,
                        round=False)
                    if quantity < out_quantity:
                        outgoing_moves.extend(Move.copy([out_move], default={
                                    'quantity': out_quantity - quantity,
                                    }))
                        Move.write([out_move], {
                                'quantity': quantity,
                                })
                    Move.write([out_move], {
                            'anmat_unit': move.anmat_unit.id,
                            })
                    quantity -= out_quantity
                assert quantity <= 0

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, shipments):
        super(ShipmentOut, cls).done(shipments)
        cls.anmat_send(shipments)

    @classmethod
    def anmat_send(cls, shipments):
        pass
        ws = AnmatTraceWS()
        for shipment in shipments:
            ws.send(shipment)


class ShipmentInReturn(ShipmentAnmatMixin):
    __name__ = 'stock.shipment.in.return'
    '''
    'supplier' and 'delivery_address' fields are missing until
    Tryton 4.0; this code should be removed from that version on
    '''
    supplier = fields.Many2One('party.party', 'Supplier',
        states={
            'readonly': (((Eval('state') != 'draft')
                    | Eval('moves', [0]))
                    & Eval('supplier', 0)),
            }, required=True,
        depends=['state', 'supplier'])
    delivery_address = fields.Many2One('party.address', 'Delivery Address',
        states={
            'readonly': Eval('state') != 'draft',
            },
        domain=[
            ('party', '=', Eval('supplier'))
            ],
        depends=['state', 'supplier'])
    anmat_reason = fields.Selection([
        ('return', 'Return'),
        ('expiration', 'Return due to expiration'),
        ('prohibition', 'Return due to prohibition'),
        ('quarantine', 'Return due to quarantine'),
        ], 'Return reason',
        states={
            'readonly': (Eval('state') != 'draft'),
            },
        depends=['state'])

    @staticmethod
    def default_anmat_reason():
        return 'return'

    @fields.depends('supplier')
    def on_change_supplier(self):
        if self.supplier:
            self.delivery_address = self.supplier.address_get('delivery')
            self.to_location = self.supplier.supplier_location


class ShipmentOutReturn(ShipmentAnmatMixin):
    __name__ = 'stock.shipment.out.return'

    anmat_reason = fields.Selection([
        ('return', 'Return'),
        ('expiration', 'Return due to expiration'),
        ('prohibition', 'Return due to prohibition'),
        ('quarantine', 'Return due to quarantine'),
        ], 'Return reason',
        states={
            'readonly': (Eval('state') != 'draft'),
            },
        depends=['state'])

    @staticmethod
    def default_anmat_reason():
        return 'return'

    @classmethod
    def _get_inventory_moves(cls, incoming_move):
        move = super(ShipmentOutReturn,
            cls)._get_inventory_moves(incoming_move)
        if move and incoming_move.anmat_unit:
            move.anmat_unit = incoming_move.anmat_unit
        return move


class ShipmentInternal(ShipmentAnmatMixin):
    __name__ = 'stock.shipment.internal'

    anmat_reason = fields.Selection([
        ('', ''),
        ('damaged', 'Damaged code or product'),
        ('stolen_missing', 'Stolen or missing'),
        ('prohibition', 'Destroyed because of prohibition'),
        ('expiration', 'Destroyed because of expiration'),
        ], 'Loss reason',
        states={
            'readonly': (Eval('state') != 'draft'),
            },
        depends=['state'])


class Period (metaclass = PoolMeta):
    __name__ = 'stock.period'
    anmat_unit_caches = fields.One2Many('stock.period.cache.anmat_unit',
        'period', 'ANMAT Unit Caches', readonly=True)

    @classmethod
    def groupings(cls):
        return super(Period, cls).groupings() + [
            ('product', 'lot', 'anmat_unit')]

    @classmethod
    def get_cache(cls, grouping):
        pool = Pool()
        Cache = super(Period, cls).get_cache(grouping)
        if grouping == ('product', 'lot', 'anmat_unit'):
            return pool.get('stock.period.cache.anmat_unit')
        return Cache


class PeriodCacheUnit(ModelSQL, ModelView):
    '''
    Stock Period Cache per ANMAT Unit

    It is used to store cached computation of stock quantities per unit.
    '''
    __name__ = 'stock.period.cache.anmat_unit'

    period = fields.Many2One('stock.period', 'Period', required=True,
        readonly=True, select=True, ondelete='CASCADE')
    location = fields.Many2One('stock.location', 'Location', required=True,
        readonly=True, select=True, ondelete='CASCADE')
    product = fields.Many2One('product.product', 'Product', required=True,
        readonly=True, ondelete='CASCADE')
    lot = fields.Many2One('stock.lot', 'Lot', readonly=True,
        ondelete='CASCADE')
    anmat_unit = fields.Many2One('stock.lot.unit', 'Unit', readonly=True,
        ondelete='CASCADE')
    internal_quantity = fields.Float('Internal Quantity', readonly=True)


class Inventory (metaclass = PoolMeta):
    __name__ = 'stock.inventory'

    @classmethod
    def grouping(cls):
        return super(Inventory, cls).grouping() + ('anmat_unit', )


class InventoryLine (metaclass = PoolMeta):
    __name__ = 'stock.inventory.line'
    anmat_unit = fields.Many2One('stock.lot.unit', 'Unit',
        domain=[
            ('lot', '=', Eval('lot')),
            ],
        depends=['lot'])

    @classmethod
    def __setup__(cls):
        super(InventoryLine, cls).__setup__()
        cls._order.insert(2, ('anmat_unit', 'ASC'))

    def get_rec_name(self, name):
        rec_name = super(InventoryLine, self).get_rec_name(name)
        if self.anmat_unit:
            rec_name += ' - %s' % self.anmat_unit.rec_name
        return rec_name

    def get_move(self):
        move = super(InventoryLine, self).get_move()
        if move:
            move.anmat_unit = self.anmat_unit
        return move
